package com.example.java.tutorial;



import com.example.java.tutorial.domain.entity.Customer;
import com.example.java.tutorial.service.CustomerRegistrationValidator;
import com.example.java.tutorial.service.CustomerService;
import com.example.java.tutorial.service.ServicePerson;
import com.example.java.tutorial.service.StreamLearn;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

import static com.example.java.tutorial.service.CustomerRegistrationValidator.*;


@SpringBootApplication
@RequiredArgsConstructor
public class JavaTutorialApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JavaTutorialApplication.class, args);
    }

    private final ServicePerson servicePerson;
    private final StreamLearn streamLearn;
    private final CustomerService customerService;

    @Override
    public void run(String... args) throws Exception {
//        servicePerson.getAllMale();
//        servicePerson.getAllFemale();
//        servicePerson.getAllAgeAsc();
//        servicePerson.getAgeGender();
//        servicePerson.getMaxAge();

/*
  New Course Java Functional Programing
 */
//        servicePerson.addFemaleWithImper();
//        servicePerson.addFemaleWithDeclar();
//        servicePerson.filterWithPredicate();
//        servicePerson._function();
//        servicePerson.functionStandard(5);
//        servicePerson.biFunction(5, 10);
//        Person people = new Person(" Zaur", 44, Gender.MALE);
//        servicePerson.greetPerson(people);
//        servicePerson.greetPersonConsumer.accept(people);
//        servicePerson.greetPersonFunction(people, false);
//        servicePerson.greetPersonBiConsumer.accept(people, false);
//        boolean phoneNumberValid = servicePerson.isPhoneNumberValid("48733032330");
//        boolean phoneNumberStartNumberNotValid = servicePerson.isPhoneNumberValid("49733032330");
//        boolean phoneNumberLengthNotValid = servicePerson.isPhoneNumberValid("4873303233");
//
//        boolean phoneNumberValidPre = servicePerson.isPhoneNumberValidPredicate.test("48733032330");
//        boolean phoneNumberStartNumberNotValidPre = servicePerson.isPhoneNumberValidPredicate.test("49733032330");
//        boolean phoneNumberLengthNotValidPre = servicePerson.isPhoneNumberValidPredicate.test("4873303233");
//
//        System.out.println("Standard Function");
//        System.out.println("Phone Number Valid : " + phoneNumberValidPre);
//        System.out.println("Phone Number Start Number NotValid : "+ phoneNumberStartNumberNotValidPre);
//        System.out.println("Phone Number Length NotValid : " + phoneNumberLengthNotValid);
//
//        System.out.println(" ");
//        System.out.println("Predicate Function");
//        System.out.println("Phone Number Valid Predicate : " + phoneNumberValidPre);
//        System.out.println("Phone Number Start Number NotValid Predicate : "+ phoneNumberStartNumberNotValidPre);
//        System.out.println("Phone Number Length NotValid Predicate : " + phoneNumberLengthNotValidPre);
//
//
//        boolean phoneNumberAndContainValidPre = servicePerson.isPhoneNumberValidPredicate.and(servicePerson.contains3Number).test("48733032330");
//        boolean phoneNumberOrContainValidPre = servicePerson.isPhoneNumberValidPredicate.or(servicePerson.contains3Number).test("48733032777");
//        boolean phoneNumberAndContainNotValidPre = servicePerson.isPhoneNumberValidPredicate.test("48733032555");


//        System.out.println(" ");
//        System.out.println("Contain");
//        System.out.println("Phone Number And Contain ValidPre : " + phoneNumberAndContainValidPre);
//        System.out.println("Phone Number Or Contain ValidPre : "+ phoneNumberOrContainValidPre);
//        System.out.println("phone Number And Contain NotValidPre : " + phoneNumberAndContainNotValidPre);
//
//        System.out.println("Standard Java Function : " + servicePerson.getDbConnectURL());
//        System.out.println("Supplier : " + servicePerson.getDbConnectURLSupplier.get());
//        System.out.println("Supplier This from Standard Function: " + servicePerson.getDbConnectURLSupplierThis.get());
//        System.out.println("Supplier List : " + servicePerson.getDbConnectURLSupplierList.get());
//////////////////////////////STREAM////////////////////////////////////////////////////

//        streamLearn.setPersonName();
//        streamLearn.personNameLength();
//        streamLearn.genderEquals();
//        streamLearn.optionalOfNullable();
//        streamLearn.optionalIfPresent();

        Customer customer = new Customer(
                "Zaur",
                "zaurjabrailov@gmail.com",
                "+48733032330",
                LocalDate.of(1977,11,20)

        );

        //if valid we can store customer in DB.
//        System.out.println(customerService.isValid(customer));

        //Using combinator pattern
        ValidationResult result = isEmailValid()
                .and(isPhoneNumberValid())
                .and(isAdultValid())
                .apply(customer);
        System.out.println(result);

        if (result != ValidationResult.SUCCESS){
            throw new IllegalArgumentException(result.name());
        }

    }


}


