package com.example.java.tutorial.domain.entity;

import com.example.java.tutorial.domain.enumeration.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String  name;
    private Integer age;
    private Gender gender;



}
