package com.example.java.tutorial.domain.entity;

import java.util.List;

import static com.example.java.tutorial.domain.enumeration.Gender.FEMALE;
import static com.example.java.tutorial.domain.enumeration.Gender.MALE;


public class ListPerson {
    public List<Person> getPerson() {
        return List.of(
                new Person("James Bond", 20, MALE),
                new Person("Alina Swith", 33, FEMALE),
                new Person("Helen White", 57, FEMALE),
                new Person("Alex Boz", 14, MALE),
                new Person("Jamie Goa", 99, MALE),
                new Person("Anna Cook", 7, FEMALE),
                new Person("Zelda Brown", 120, FEMALE)
        );

    }
}
