package com.example.java.tutorial.domain.enumeration;

public enum Gender {
    MALE, FEMALE
}
