package com.example.java.tutorial.service;

import com.example.java.tutorial.domain.entity.ListPerson;
import com.example.java.tutorial.domain.entity.Person;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Collectors;

import static com.example.java.tutorial.domain.enumeration.Gender.FEMALE;

@Component
public class StreamLearn {

    ListPerson listPerson = new ListPerson();
    List<Person> people = listPerson.getPerson();

    public void setPersonName() {

        List<Person> people = listPerson.getPerson();
        people.stream()
                .map(Person::getName)
                .collect(Collectors.toSet())
                .forEach(System.out::println);
    }

    public void personNameLength() {

        Function<Person, String> getName = Person::getName;  // OR // = person -> person.name
        ToIntFunction<String> length = String::length;       // OR // = value -> value.length()
        IntConsumer println = System.out::println;           // OR // = pri -> System.out.println(pri)

        people.stream()
                .map(getName)
                .mapToInt(length)
                .forEach(println);
    }

    public void genderEquals() {
        boolean containsOnlyFemale = people.stream()
//                .allMatch(person -> FEMALE.equals(person.getGender()));
//                .anyMatch(person -> FEMALE.equals(person.getGender()));
                .noneMatch(person -> FEMALE.equals(person.getGender()));


        System.out.println(containsOnlyFemale);
    }

    public void optionalOfNullable() {
        Supplier<IllegalStateException> info_exception = () -> new IllegalStateException("Info Exception");
        Object values = Optional.ofNullable(null)
//                .orElseGet(() -> "Default value");
                .orElseThrow(info_exception);
        System.out.println(values);

    }

    public void optionalIfPresent() {
//        Optional.ofNullable("Hello").ifPresent(System.out::println);
//        Optional.ofNullable("zaurjabrailov@gmail.com").ifPresent(email -> System.out.println("Sending email : " +email) );
        Optional.ofNullable(null).ifPresentOrElse(
                email -> System.out.println("Sending email : " +email),
                () -> System.out.println("Cannot send Email")
                );
    }


}
