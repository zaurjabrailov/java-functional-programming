package com.example.java.tutorial.service;

public class FirstStep {

        /*
        //Imperative approach  X Dont like
         List<Person> people = getPerson();
        List<Person> families = new ArrayList<>();
        for( Person person : people){
            if (person.getGender().equals(Gender.FEMALE)){
                families.add(person);
            }
        }
        families.forEach(System.out::println);
*/

/*
        //Declarative approach
         List<Person> people = getPerson();
        List<Person> females = people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .collect(Collectors.toList());
        females.forEach(System.out::println);
*/

/*
        //Sort

        List<Person> people = getPerson();
        List<Person> sorted = people.stream()
                .sorted(Comparator
                        .comparing(Person::getAge)
                        .thenComparing(Person::getGender)
                        .reversed())
                .collect(Collectors.toList());
        sorted.forEach(System.out::println);
*/

/*

        //All Match ( match uygun gelmek)

        List<Person> people = getPerson();
        boolean allMatch = people.stream()
                .allMatch(person -> person.getAge() > 5);
        System.out.println(allMatch);
*/

/*
        //Any Match
        List<Person> people = getPerson();
        boolean anyMatch = people.stream()
                .anyMatch(person -> person.getAge() > 121);
        System.out.println(anyMatch);
*/

/*
        //None Match
        List<Person> people = getPerson();
        boolean noneMatch = people.stream()
                .noneMatch(person -> person.getName().equals("James Bond"));
        System.out.println(noneMatch);
*/

/*
        //Max
        List<Person> people = getPerson();
        Optional<Person> max = people.stream()
                .max(Comparator.comparing(Person::getAge));
        System.out.println(max);
*/

//        Person person = people.stream().max(Comparator.comparing(Person::getAge)).get();
//        System.out.println(person);

//        people.stream()
//                .max(Comparator.comparing(Person::getAge)).ifPresent(System.out::println);


/*
        //Max
        List<Person> people = getPerson();
        people.stream()
                .min(Comparator.comparing(Person::getAge)).ifPresent(System.out::println);
*/

/*
        //Group
        List<Person> person = getPerson();

        Map<Gender, List<Person>> groupByGender = person.stream()
                .collect(Collectors.groupingBy(Person::getGender));

        groupByGender.forEach((gender, people) -> {
            System.out.println(gender);
            Optional<String> olderFemaleAge = people.stream()
                    .filter(people2 -> people2.getGender().equals(Gender.FEMALE))
                    .max(Comparator.comparing(Person::getAge))
                    .map(Person::getName);
            olderFemaleAge.ifPresent(System.out::println);

            Optional<String> olderMaleAge = people.stream()
                    .filter(people1 -> people1.getGender().equals(Gender.MALE))
                    .max(Comparator.comparing(Person::getAge))
                    .map(Person::getName);

            olderMaleAge.ifPresent(System.out::println);

        });
*/





//    private static List<Person> getPerson() {
//        return List.of(
//                new Person("James Bond", 20, Gender.MALE),
//                new Person("Alina Swith", 33, Gender.FEMALE),
//                new Person("Helen White", 57, Gender.FEMALE),
//                new Person("Alex Boz", 14, Gender.MALE),
//                new Person("Jamie Goa", 99, Gender.MALE),
//                new Person("Anna Cook", 7, Gender.FEMALE),
//                new Person("Zelda Brown", 120, Gender.FEMALE)
//        );
//
//    }


}
