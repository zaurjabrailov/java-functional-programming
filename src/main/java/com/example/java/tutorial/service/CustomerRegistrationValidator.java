package com.example.java.tutorial.service;

import com.example.java.tutorial.domain.entity.Customer;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

import static com.example.java.tutorial.service.CustomerRegistrationValidator.*;
import static com.example.java.tutorial.service.CustomerRegistrationValidator.ValidationResult.*;

public interface CustomerRegistrationValidator
        extends Function<Customer, ValidationResult> {

    static CustomerRegistrationValidator isEmailValid() {
        return customer -> customer.getEmail().contains("@") ?
                SUCCESS : EMAIL_NOT_VALID;
    }

    static CustomerRegistrationValidator isPhoneNumberValid() {
        return customer -> customer.getPhoneNumber().startsWith("+48") ?
                SUCCESS : PHONE_NUMBER_NOT_VALID;
    }

    static CustomerRegistrationValidator isAdultValid() {
        return customer ->
                Period.between(customer.getDob(), LocalDate.now()).getYears() > 18 ?
                        SUCCESS : PHONE_NUMBER_NOT_VALID;
    }

    default CustomerRegistrationValidator and (CustomerRegistrationValidator other){
        return customer -> {
            ValidationResult result = this.apply(customer);
            return result.equals(SUCCESS) ? other.apply(customer) : result;
        };
    }

    enum ValidationResult {
        SUCCESS,
        PHONE_NUMBER_NOT_VALID,
        EMAIL_NOT_VALID,
        IS_NOT_AN_ADULT
    }
}
