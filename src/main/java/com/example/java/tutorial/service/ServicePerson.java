package com.example.java.tutorial.service;


import com.example.java.tutorial.domain.entity.Person;
import com.example.java.tutorial.domain.enumeration.Gender;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class ServicePerson {

//    List<Person> people = getPerson();
/*
    public void getAllMale() {

        List<Person> personList = people.stream()
                .filter(person1 -> person1.getGender().equals(Gender.MALE))
                .collect(Collectors.toList());
        personList.forEach(System.out::println);
    }

    public void getAllFemale() {
        List<Person> female = people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .collect(Collectors.toList());
        female.forEach(System.out::println);
    }

    public void getAllAgeAsc() {
        List<Person> collect = people.stream()
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());
        collect.forEach(System.out::println);

    }

    public void getAgeGender() {
        List<Person> personList = people.stream()
                .sorted(Comparator.comparing(Person::getAge)
                                .thenComparing(Person::getGender)
//                        .thenComparing(Person::getName)
                )
                .collect(Collectors.toList());
//        personList.forEach(System.out::println);
        for (Person p : personList) {
            System.out.println(p);
        }
    }

    public void getMaxAge() {
        Optional<Person> max = people.stream()
                .max(Comparator.comparing(Person::getAge));
        Stream<String> stringStream = max.stream()
                .map(Person::getName);
        stringStream.map(String::toUpperCase);
        stringStream.forEach(System.out::println);
    }
*/

    /**
     * New Course Java Functional Programing
     */
/*
    // Imperative approach
    public void addFemaleWithImper() {
//all Female collect to females List
        System.out.println("Imperative approach");
        List<Person> females = new ArrayList<>();
        for (Person person : people) {
            if (person.getGender().equals(Gender.FEMALE)) {
                females.add(person);
            }
        }
//Print from collect  females List
        for (Person female : females) {
            System.out.println(female);
        }
    }

    // Declarative approach
    public void addFemaleWithDeclar() {
//Filter the same if, collect the same add to list, foreach the same Print
        System.out.println(" ");
        System.out.println("Declarative approach");
//Print version 1 the best sort version
        people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .collect(Collectors.toList())
                .forEach(System.out::println);

//Print version 2 addition Reference
//        List<Person> personList = people.stream()
//                .filter(person -> person.getGender().equals(Gender.FEMALE))
//                .collect(Collectors.toList());
//        personList.forEach(System.out::println);
    }
*/
/*
    public void filterWithPredicate() {
        Predicate<Person> personPredicate = person -> person.getGender().equals(Gender.FEMALE);
        people.stream()
                .filter(personPredicate)
                .collect(Collectors.toList())
                .forEach(System.out::println);
    }
*/

/*
//Function
    public void _function() {
        int increment = incrementByOne(5);
        System.out.println(increment);

        int incrementWithFun = incrementByOneWithFunction.apply(10);
        System.out.println(incrementWithFun);

        int multiplyWithFun = multiplyByTenFunction.apply(incrementWithFun);
        System.out.println(multiplyWithFun);


        Function<Integer, Integer> addIncrementThenMultiply = incrementByOneWithFunction.andThen(multiplyByTenFunction);
        Integer by1ThenMultiplyBy10 = addIncrementThenMultiply.apply(5);
        System.out.println(by1ThenMultiplyBy10);
    }

    public Integer incrementByOne(int number) {
        return number + 1;
    }

    Function<Integer, Integer> incrementByOneWithFunction = number -> number + 1;
    Function<Integer, Integer> multiplyByTenFunction = number -> number * 10;
*/


/*

    //BiFunction
    //Standard Java
    //out 5 + 1 = 6
    public void functionStandard(int number){
        System.out.println("Standard Function ");
        System.out.println(incrementByOneAndMultiply(number));
    }

    public int incrementByOneAndMultiply(int number){
        return number +1;
    }

    public void biFunction(int numberForIncrement, int numberForMultiply){
        System.out.println(" ");
        System.out.println("BiFunction ");
        System.out.println(
                biFunction.apply( numberForIncrement,  numberForMultiply));
    }

//Out 5 + 5 * Out 10 = 100
    static BiFunction<Integer, Integer, Integer> biFunction = (numberToIncrementByOne, numberToMultiplyBy) -> (numberToIncrementByOne + 5) * numberToMultiplyBy;

*/
/*
//Consumer
    public void greetPerson( Person person){
        System.out.println("Hello" + person.getName() +", age is " + person.getAge() + " gender " +person.getGender());
    }

    public Consumer<Person> greetPersonConsumer = person ->
            System.out.println("Hello" + person.getName() +", age is " + person.getAge() + " gender " +person.getGender());

*/

/*
//BiConsumer

    public void greetPersonFunction(Person person, Boolean dontShowAge){
        System.out.println("Hello" + person.getName() +", age is " + (dontShowAge ? person.getAge() : "*******") + " gender " +person.getGender());
    }


    public BiConsumer<Person, Boolean> greetPersonBiConsumer = (person, dontShowAge) ->
            System.out.println("Hello" + person.getName() +", age is " + (dontShowAge ? person.getAge() : "*******") + " gender " +person.getGender());

*/

/*
//Predicate
    public boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.startsWith("48") && phoneNumber.length() == 11;
    }

    public Predicate<String> isPhoneNumberValidPredicate = phoneNumberPre ->
            phoneNumberPre.startsWith("48") && phoneNumberPre.length() == 11;


    public Predicate<String> contains3Number = phoneNumberPre ->
            phoneNumberPre.contains("0");
*/

/*
    //Supplier

    public String  getDbConnectURL (){
        return "jdbc://localhost:8080/users";
    }

    public Supplier<String> getDbConnectURLSupplier = () -> "jdbc://localhost:8080/users";
    public Supplier<String> getDbConnectURLSupplierThis = this::getDbConnectURL;
    public Supplier<List<String>> getDbConnectURLSupplierList = ()
            -> List.of("jdbc://localhost:8080/users",
            "jdbc://localhost:8081/courses",
            "jdbc://localhost:8082/auth");
*/

}
