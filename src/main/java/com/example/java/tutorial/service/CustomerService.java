package com.example.java.tutorial.service;

import com.example.java.tutorial.domain.entity.Customer;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;

@Service
public class CustomerService {

    private boolean isEmailValid(String email){
        return email.contains("@");
    }

    private boolean isPhoneNumber(String phoneNumber){
        return phoneNumber.startsWith("+48");
    }

    private boolean isAdult(LocalDate dob){
        return Period.between(dob,LocalDate.now()).getYears() > 18;
    }

    public boolean isValid(Customer customer){
        return isEmailValid(customer.getEmail()) &&
                isPhoneNumber(customer.getPhoneNumber()) &&
                isAdult(customer.getDob());
    }

}
